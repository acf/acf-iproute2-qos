local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstopinterface(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.details(self)
	return self.model.listinterfacedetails(self)
end

function mymodule.enable(self)
	return self.handle_form(self, self.model.get_enable, self.model.enable, self.clientdata, "Enable", "Enable QOS on Interface", "Enabled QOS on Interface")
end

function mymodule.config(self)
	return self.handle_form(self, self.model.get_config, self.model.update_config, self.clientdata, "Save", "Edit QOS Config", "Configuration Set")
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filedetails, self.clientdata, "Save", "Edit QOS Config", "Configuration Set")
end

return mymodule
