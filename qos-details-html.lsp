<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"enable","config","expert","startstopinterface"}, session) %>

<% viewlibrary.dispatch_component("status") %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Interface</th>
		<th>Status</th>
	</tr>
</thead><tbody>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,intf in ipairs(data.value) do %>
	<tr>
		<td>
		<% if intf.enabled then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={DEV=cfe({type="hidden", value=intf.interface}), redir=redir}, label="", option="Edit", action="config"}), page_info, -1) %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={filename=cfe({type="hidden", value=intf.filename}), redir=redir}, label="", option="Expert", action="expert"}), page_info, -1) %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={servicename=cfe({type="hidden", value=intf.init}), redir=redir}, label="", option="Autostart", action=page_info.script.."/alpine-baselayout/rc/edit"}), page_info, -1) %>
		<% else %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={interface=cfe({type="hidden", value=intf.interface})}, label="", option="Enable", action="enable"}), page_info, -1) %>
		<% end %>
		</td>
		<td><%= html.html_escape(intf.interface) %></td>
		<td><%= html.html_escape(intf.status) %></td>
		<% if intf.enabled then %>
			<td>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={init=cfe({type="hidden", value=intf.init})}, label="", option={"Start", "Stop", "Restart"}, action="startstopinterface"}), page_info, -1) %>
			</td>
		<% end %>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level) %>
